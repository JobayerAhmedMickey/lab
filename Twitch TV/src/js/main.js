$(document).ready(function(){
	var url = "http://wind-bow.glitch.me/twitch-api/streams/freecodecamp?callback=?";

	$.getJSON(url, function(data){
		if(data.stream === null){
			$('#fcc').text("Offline");
			$('.channels1').addClass('offline');
			$('.channels1').removeClass('online');
		} else{
			$('#fcc').text("Online");
			$('.channels1').addClass('online');
			$('.channels1').removeClass('offline');
		}

	});

	var url1 = 'http://wind-bow.glitch.me/twitch-api/streams/blizzheroes?callback=?';

	$.getJSON(url1, function(data){
      	//console.log(data)
      	if(data.stream === null){
			$('#blizzheroes').text("Offline");
			$('.channels2').addClass('offline');
			$('.channels2').removeClass('online');
		} else{
			$('#blizzheroes').text("Online");
			$('.channels2').addClass('online');
			$('.channels2').removeClass('offline');
		}
    });

    var url2 = "http://wind-bow.glitch.me/twitch-api/streams/scgtour?callback=?";

	$.getJSON(url2, function(data){
		if(data.stream === null){
			$('#scgtour').text("Offline");
			$('.channels3').addClass('offline');
			$('.channels3').removeClass('online');
		} else{
			$('#scgtour').text("Online");
			$('.channels3').addClass('online');
			$('.channels3').removeClass('offline');
		}

	});

	var url3 = "http://wind-bow.glitch.me/twitch-api/streams/wraxu?callback=?";

	$.getJSON(url3, function(data){
		if(data.stream === null){
			$('#wraxu').text("Offline");
			$('.channels4').addClass('offline');
			$('.channels4').removeClass('online');
		} else{
			$('#wraxu').text("Online");
			$('.channels4').addClass('online');
			$('.channels4').removeClass('offline');
		}

	});

	var url4 = "http://wind-bow.glitch.me/twitch-api/streams/blackfireice?callback=?";

	$.getJSON(url4, function(data){
		if(data.stream === null){
			$('#blackfireice').text("Offline");
			$('.channels5').addClass('offline');
			$('.channels5').removeClass('online');
		} else{
			$('#blackfireice').text("Online");
			$('.channels5').addClass('online');
			$('.channels5').removeClass('offline');
		}

	});

	var url5 = "http://wind-bow.glitch.me/twitch-api/streams/lpmassive?callback=?";

	$.getJSON(url5, function(data){
		if(data.stream === null){
			$('#lpmassive').text("Offline");
			$('.channels6').addClass('offline');
			$('.channels6').removeClass('online');
		} else{
			$('#lpmassive').text("Online");
			$('.channels6').addClass('online');
			$('.channels6').removeClass('offline');
		}

	});

	//Add Click Event

	$('#all').click(function(){
		$('.channel div').removeClass('hide');
	});
	$('#on').click(function(){
		$('.offline').addClass('hide');
		$('.online').removeClass('hide');
	});
	$('#off').click(function(){
		$('.online').addClass('hide');
		$('.offline').removeClass('hide');
	});
});




/*
scgtour
wraxu
blackfireice
lpmassive
*/