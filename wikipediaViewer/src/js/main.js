$(document).ready(function() {
	$('#search').click(function() {
		var searchvalue = $('#searchValue').val();

		var url = "https://en.wikipedia.org/w/api.php/?action=opensearch&search=" + searchvalue + "&format=json&callback=?";

		$.ajax({
			type: "GET",
			url: url,
			async: false,
			dataType: "json",
			success: function (data) {
				$('.content').html('');
				for (var i = 0; i < data[1].length; i++) {
					$('.content').prepend("<a href= " +data[3][i]+ "><section><div><h2>" +data[1][i]+ "</h2><p>" +data[2][i]+ "</p></div></section></a>");
				}
			},
			error: function (errorMessage) {
				alert('No Result Found');
			}
		})
	});
});