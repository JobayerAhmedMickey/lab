$(document).ready(function() {
	
	// Api Section

	function getWeather() {
		var api = 'http://api.openweathermap.org/data/2.5/weather?q=Dhaka,BD&appid=0c520a858879b1e768b3042d4693e7f3';

		$.getJSON(api, function (tempData) {
			var x = tempData.main.temp - 273.15;
			var y = tempData.clouds.all;
			$('.temp').text(x);
			if (y <= 15) {
				$('.herotext').text("Soo... Wet");
				$('.winter').removeClass('hide');
				$('.sunny').addClass('hide');
				$('.rainy').addClass('hide');
			}
			else if (y > 15 && y <= 25) {
				$('.herotext').text("Complete Gray");
				$('.rainy').removeClass('hide');
				$('.sunny').addClass('hide');
				$('.winter').addClass('hide');
			} else {
				$('.herotext').text("Not Too Sunny");
				$('.sunny').removeClass('hide');
				$('.rainy').addClass('hide');
				$('.winter').addClass('hide');
			}
		});
	};
	getWeather();

	// Get Date Name

	var weekday=new Array(7);
	weekday[0]="Sunday";
	weekday[1]="Monday";
	weekday[2]="Tuesday";
	weekday[3]="Wednesday";
	weekday[4]="Thursday";
	weekday[5]="Friday";
	weekday[6]="Saturday";

	var d = new Date();
	var dayName = weekday[d.getDay()];

	// Get Month Name

	var months = [ "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec" ];

	var selectedMonthName = months[d.getMonth()];

	// Get Date
	
	var date = new Date();
	var currentDate = d.getDate();

	// Show Date Time in Markup

	$('.showDateTime').text(dayName + ", " + selectedMonthName + " " + currentDate);
});